:syntax on
:set autoindent
:set tabstop=2
:set smarttab
:set shiftwidth=2
:set hlsearch
:set ignorecase
:set lazyredraw
:set laststatus=2
:set ruler
:set cursorline
:set number
:set noerrorbells
:set visualbell
:set title
:set mouse=a
:set backspace=indent,eol,start
:set linebreak
