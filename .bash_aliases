# Common Conveniences
alias _="sudo "
alias ls="ls -l --color=auto"
alias la="ls -lah --color=auto"
alias ..="cd .."
alias ...="cd ../.."
alias svim="sudo vim "
# Convenient Systemd Commands
alias sstatus="sudo systemctl status "
alias senable="sudo systemctl enable "
alias sdisable="sudo systemctl disable "
alias sstart="sudo systemctl start "
alias sstop="sudo systemclt stop "
alias slog="sudo journalctl -u "
alias sloga="sudo journalctl -xe"
# Package Conveniences(Debian Based!)
alias ainst="sudo apt install -y --no-install-recommends"
alias aupg="sudo apt update && sudo apt upgrade -y && sudo apt autoremove"
alias aupd="sudo apt update"
